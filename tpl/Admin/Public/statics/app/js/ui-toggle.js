+function ($) {

  $(function(){

      $(document).on('click', '[ui-toggle]', function (e) {
      	e.preventDefault();
        var $this = $(e.target);
        $this.attr('ui-toggle') || ($this = $this.closest('[ui-toggle]'));
        var $target = $($this.attr('target')) || $this;
        $target.toggleClass($this.attr('ui-toggle'));
      });
      
      // 动态调整iframe的高度以适应不同高度的显示器
      $('.app-content-body').height($(window).height()-100);
      // $('.app-content-body').css('padding-bottom',50);
  });
}(jQuery);