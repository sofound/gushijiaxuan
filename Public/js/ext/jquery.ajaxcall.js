/**
 * 地址，数据，回调函数，传送数据方式，返回数据类型，同步或者异步
 * 默认为同步
 */
jQuery.ajaxcall = function(url,data,callback,type,dataType,async){
	var error = {'url':'请求地址错误！','type':'传送类型错误!','dataType':'数据类型错误!'};
	var dataType_arr = ['POST','GET'];
	var type_arr = ['txt','json'];
	if(!url || typeof(url)=='undefined'){
		alert(error['url']);
	}
	if(!type) type = 'POST';
	if(!dataType) dataType = 'json';
	if(!async) async = false;
	jQuery.ajax({
		type:type,
		url:url,
		data:data,
		async:async,
		dataType:dataType,
		success:function(res){
			if(typeof(callback)=='function'){
				callback(res);
			}else{
				alert(callback);
			}
		}
//	,
//		error : function(res, error){
//			alert("ajax call fail." + res + "\nerror:" + error);
//		}
	});
};