<?php
namespace Login\Controller;
use Common\Controller\WechatController;
use Think\Log;
use Org\wechat\Wechat;
use Common\Controller\HomeBaseController;
class IndexController extends HomeBaseController{
	
    	public function index(){
    		$map['type'] = 'webInfo';
    		$webInfo = D('Config')->getData($map);
    		$this->assign('webInfo', $webInfo);
    		$this->display('Home/login');
    	}

        public function login(){
            $this->display('Home/login');
        }
        public function logout(){
            session('name',null);
            session('[destroy]');
            $this->display('Home/login');    
        }

    	//普通登录
       
    	public function log(){
    		$username = I('post.username');
    		$password = I('post.password');
            if (!$username) {
                $error_message = '请输入用户名';
                 $this->assign('error_message',$error_message);
                 $this->display('Home/login');
                exit;
                # code...
            }
    		$data = D('users')->where(array('username'=>$username,'password'=>md5($password)))->find();
    		if($data){
    			$_SESSION['name'] = $data['name'];
                $_SESSION['user_id'] = $data['id'];
                $_SESSION['username'] = $data['username'];
                $_SESSION['phone'] = $data['phone'];
                $_SESSION['profession']= $data['profession'];

                $this->redirect('/User/Index/ucinfo');
                 exit;
    		}else{
    			$error_message = '登陆失败';
                 $this->assign('error_message',$error_message);
                 $this->display('Home/login');
    		}
    	}
        
    	/**
         * 普通注册
         * Enter description here ...
         */
        public function register(){
         	$this->display('Home/register');
         }
        public function reg(){
        	$reg = I('post.');

        	$username=$reg['username'];
        	if (!$username) {
        		$message = '请输入邮箱';
        		 $this->assign('message',$message);
        		 $this->display('Home/register');
        		exit;
        		# code...
        	}
        	$password=$reg['password'];
           // {$password|mb_substr=0,15}
        	if (!$password) {
        		$message = '请输入密码';
        		$this->assign('username',$username);
                $this->assign('password',$password);
                $this->assign('password_check',$password_check);
                $this->assign('message',$message);
                $this->assign('name',$name);
        		 $this->display('Home/register');
        		exit;
        		# code...
        	}
            if ( strlen($password) <6 ||strlen($password) >8) {
                $message = '密码在长度6-8位';
                $this->assign('username',$username);
                 $this->assign('message',$message);
                 $this->display('Home/register');
                exit;
                # code...
            }
        	$password_check=$reg['password_check'];
        	if ($password_check!=$password) {
        		$message = '两次密码输入不相同';
        		$this->assign('username',$username);
        		$this->assign('password',$password);
        		 $this->assign('message',$message);
        		 $this->display('Home/register');
        		exit;
        		# code...
        	}
            $_SESSION['text_password']= $reg['password']; 
        	$name=$reg['name'];
        	if (!preg_match("/^[\x{4e00}-\x{9fa5}]+$/u",$name)) {
        		$message = '姓名是中文';
        		$this->assign('username',$username);
        		$this->assign('password',$password);
        		$this->assign('password_check',$password_check);
        		$this->assign('message',$message);
        		$this->display('Home/register');
        		exit;
        		# code...
        	}
           
        	$phone=$reg['phone'];
        	if (!preg_match("/^1[34578]{1}\d{9}$/",$phone)) {
        		$message = '请输入正确的手机号码格式';
        		$this->assign('username',$username);
        		$this->assign('password',$password);
        		$this->assign('password_check',$password_check);
        		$this->assign('message',$message);
        		$this->assign('name',$name);
        		$this->display('Home/register');
        		exit;
        		# code...
        	}

        	 $data = D('users')->where(array('username'=>$username))->find();
             if ($data) {
                $message = '用户名已存在';
                $this->assign('username',$username);
                $this->assign('password',$password);
                $this->assign('password_check',$password_check);
                $this->assign('message',$message);
                $this->assign('name',$name);
                $this->display('Home/register');
                exit;
                 # code...
             }
            
        	$reg['password']=md5($reg["password"]);
              
        	M('users')->add($reg);
                $_SESSION['text_username'] = $reg['username'];              
                $_SESSION['name'] = $reg['name'];             
                $_SESSION['phone'] = $reg['phone'];
                $_SESSION['profession']= $reg['profession']; 
                
        		# code...	
                # 	
    		   $this->redirect('/User/Index/ucinfo');  		
    		
        }
}