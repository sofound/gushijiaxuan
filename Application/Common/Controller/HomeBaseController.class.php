<?php
namespace Common\Controller;
use Common\Controller\BaseController;
/**
 * Home基类控制器
 */
class HomeBaseController extends BaseController{
	
	public $user_id;
	public $openid;
	/**
	 * 初始化方法
	 */
	public function _initialize(){
		parent::_initialize();
		$map['type'] = 'webInfo';
		$webInfo = D('Config')->getData($map);
		if(!$webInfo['web_is_close']){
//			C('DEFAULT_THEME','wap');
			//wap手机版单独开启检测
			if(IS_MOBILE){
	        	C('DEFAULT_THEME','wap');//模板定义到手机wap单独模板
			}
			$map['pid'] = 0;
			$navList = D('HomeNav')->where($map)->order('order_number')->select();
			$webInfo['tel'] = str_replace("-", "", $webInfo['phone']);
			if($webInfo['qq'] != null){
	        	$webInfo['qq'] = explode(',', $webInfo['qq']);
	        }
	        $aboutlist = M('Article')->where(array('cid'=>3))->select();
	        $newsList = M('ArticleCategory')->where('pid=2')->select();
	        $serviceMore = M('Article')->where('cid=14')->select();
	        if($webInfo['web_domain'] == $_SERVER['SERVER_NAME']){
	        	$this->assign('statistics_flag', true);
	        }
			$this->assign('aboutlist', $aboutlist);
			$this->assign('casesList', $casesList);
			$this->assign('newsList', $newsList);
			$this->assign('serviceMore', $serviceMore);
			$this->assign('webInfo', $webInfo);
			
			$cid=5;//合作伙伴
			$partnerlist = D('Article')->where(array('cid'=>$cid))->limit(0,4)->select();
			$this->assign('partnerlist',$partnerlist);
			
			$cid=7;//服务
			$servicelist = D('Article')->where(array('cid'=>$cid))->limit(0,4)->select();
			$this->assign('servicelist',$servicelist);
			
			//不用登录的模块
			$pubmodule = array('Home','Login','Article');

			if(!$this->islogin()){
				if(!in_array(MODULE_NAME,$pubmodule)){
					$this->toLoginPage();
				}
	    	}else{
	    		$this->user_id = $_SESSION['user_id'];
	    		$this->assign('user_id',$_SESSION['user_id']);
	    		$this->assign('name',$_SESSION['name']);
	    	}
			
		}else{
			$this->error($webInfo['web_prompt']);
		}
	}
	
	/**
     * 判断用户是否登录
     * Enter description here ...
     */
    public function islogin(){
    	return $_SESSION['user_id'];
    }
    /**
     * 登出
     * Enter description here ...
     */
    public function logout(){
    	unset($_SESSION['user_id']);
    	session_destroy();
    	$this->redirect('/');
    }
    
    /**
     * 去到登录页面
     * Enter description here ...
     */
    public function toLoginPage(){
    	$return_url = $_SERVER['REQUEST_URI'];
  		$_SESSION['login_return_url'] = $return_url;
    	$this->redirect('/login');
    }
	
}

