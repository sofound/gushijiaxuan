<?php
namespace Common\Controller;
use Think\Controller;
/**
 * Base基类控制器
 */
class BaseController extends Controller{
    /**
     * 初始化方法
     */
    public function _initialize(){
        $map=array(
			array('type'=>'webInfo'),
			);
		$webInfo = D('Config')->getData($map);
		$this->assign('webInfo', $webInfo);
		define('IS_MOBILE', is_mobile());
    }

    
}
