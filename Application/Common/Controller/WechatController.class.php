<?php
namespace Common\Controller;
use Think\Controller;
use Org\wechat\TPWechat;
/**
 * Base基类控制器
 */
class WechatController extends Controller{
    public $wechat;
    /**
     * 初始化方法
     */
    public function _initialize(){
        $this->wechat = getWechat();
    }

}
