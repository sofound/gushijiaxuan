<?php
namespace Common\Controller;
use Common\Controller\BaseController;
/**
 * admin 基类控制器
 */
class AdminBaseController extends BaseController{
	/**
	 * 初始化方法
	 */
	public function _initialize(){
		parent::_initialize();
		$user = $_SESSION['user'];
		if($user != null){
			$auth=new \Think\Auth();
			$rule_name=MODULE_NAME.'/'.CONTROLLER_NAME.'/'.ACTION_NAME;

			$authRule = D('AuthRule');
			$map['name'] = array('like',$rule_name);
			$rule = $authRule->where($map)->find();
			if($rule != null){
				$result=$auth->check($rule_name,$user['id']) || $rule_name=='Admin/Index/logout';
				if(!$result){
					$this->error('您没有权限访问');
				}
			}
			$this->assign('flag', isSuperAdmin());
		}
	}
}

