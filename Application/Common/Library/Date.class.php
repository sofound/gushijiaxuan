<?php
namespace Common\Library;
class Date{
	
	/**
	 * 获得当前时间戳
	 * Enter description here ...
	 */
	public static function time($str=''){
		return time($str);
	}
	
	public static function microtime($flag=true){
		return microtime($flag);
	}
	
	/**
	 * 时间戳转化为时间格式,去除0填充，月份：n,日：j
	 * Enter description here ...
	 */
	public static function timeToStr($format=0,$time=0){
		$time = $time?$time:Date::time();
		$format = $format?$format:'Y-m-d H:i:s';
		return date($format,$time);
	}
	
	/**
	 * 日期格式转化为时间戳
	 * Enter description here ...
	 */
	public static function strToTime($str){
		return strtotime($str);
	}
	
	/**
	 * 获得时间的描述
	 * 0 y-m-d h-i-s
	 * 1 y-m-d 
	 * Enter description here ...
	 * @param unknown_type $time
	 * @param unknown_type $type
	 */
	public static function getTimeDesc($time=0,$type=0){
		switch ($type){
			case 0://2012-02-17 17:21:32
				return Date::timeToStr(0,$time);
				break;
			case 1://2012-02-17
				return Date::timeToStr('Y-m-d',$time);
				break;
		}
	}
	
	/**
	 * 获得时间的数组
	 * Enter description here ...
	 * @param unknown_type $time
	 */
	public static function getTimeArray($time){
		if($time>0){
			$y = Date::timeToStr('Y',$time);
			$m = Date::timeToStr('m',$time);
			$d = Date::timeToStr('d',$time);
		}else{
			$y = Date::timeToStr('Y',$time);
			$m = Date::timeToStr('m',$time);
			$d = Date::timeToStr('d',$time);
		}
		$arr = array('y'=>$y,'m'=>$m,'d'=>$d);
		return $arr;
	}
	
	//获得某时间当天的凌晨时间戳
	public static function getZoneTime($time=0){
		if(!$time){
			$time = Date::time();
		}
		$day = Date::strToTime(Date::getTimeDesc($time,1));
		return $day;
	}
	
	/**
	 * 获得两个时间相差几天？
	 * Enter description here ...
	 * @param unknown_type $time1 时间戳
	 * @param unknown_type $time2 时间戳
	 */
	public static function getTimeDiff($time1,$time2){
		$t1_day = Date::getZoneTime($time1);
		$t2_day = Date::getZoneTime($time2);
		$diff = $t2_day-$t1_day;
		$diff_day = $diff/(24*60*60);
		return $diff_day;
	}
	
	/**
	 * 获得两个时间相差的天数，只算月和日
	 * Enter description here ...
	 * @param unknown_type $day1 参照日期--固定
	 * @param unknown_type $day2 对比日期-变动
	 */
	public static function getTimeDiff2($day1,$day2){
		$time1 = Date::strToTime($day1);
		$time2 = Date::strToTime($day2);
		$time1_m = Date::timeToStr('n',$time1);
		$time2_m = Date::timeToStr('n',$time2);
		if($time1_m<$time2_m){
			$day1_now = (Date::timeToStr('Y')+1).'-'.Date::timeToStr('m-d',$time1);
		}else{
			$day1_now = Date::timeToStr('Y').'-'.Date::timeToStr('m-d',$time1);
		}
		$endtime = Date::strToTime($day1_now);
		$diff = Date::getTimeDiff($time2, $endtime);
		return $diff;
	}
	
	/**
	 * 获得某个月的第一天
	 * @param $year
	 * @param $month
	 * @return unknown_type
	 */
	public static function getMonthFirstDay($year,$month){
		if(!$year){//没有年份则算当前年份
			$year = Date::timeToStr('Y',Date::time());
		}
		$time = Date::strToTime($year.'-'.$month.'-01');
		return $time;
	}
	
	/**
	 * 获得某个月的最后一天
	 * @param $year
	 * @param $month
	 * @return unknown_type
	 */
	public static function getMonthLastDay($year,$month){
		if(!$year){//没有年份则算当前年份
			$year = Date::timeToStr('Y',Date::time());
		}
		$time = Date::strToTime($year.'-'.$month.'-01 + 1 month');
		return $time;
	}
	
}