<?php
namespace Common\Library;
/** 
 * 随机数类
 * @author sofound
 */
class Random{	
 	/*
	 * 获得随机字符的用户名
	 * $i 为想获取的字符串个数
	 */
	public static function getRandomUsername($i){	
		$str = "abcdefghijklmnopqrstuvwxyz123456789ABCDEFGHIGKLMNOPQRSTUVWXYZ";
		$finalStr = "";
	  	for($j=0;$j<$i;$j++){
	    	$finalStr .= substr($str,rand(0,60),1);
	  	}
	  	return $finalStr;

	}
	
	 public static function getRandSn(){
	    /* 选择一个随机的方案 */
	    mt_srand((double) microtime() * 1000000);
	
	    return date('Ymd') . str_pad(mt_rand(1, 99999), 5, '0', STR_PAD_LEFT);
	}

	/**
	 * 获得手机验证码
	 * Enter description here ...
	 * @return unknown
	 */
	public static function getTelCode(){
		$code = substr(md5(time()), 1, 5);
		return $code;
	}
	
	/**
	 * 获得随机数字
	 * @param $len 随机数长度
	 * @return unknown_type
	 */
	public static function getRandNum($len=4){
		for($i=1;$i<=$len;$i++){
			$str .= mt_rand(0, 9);
		}
		return $str;
	}
	
}


?>