<?php
namespace Common\Model;
use Common\Model\BaseModel;
/**
 * 菜单操作model
 */
class WxKeywordModel extends BaseModel{

    // 自动验证
    protected $_validate=array(
        array('keyword','require','名称不能为空',0,'',3), // 验证字段必填
    );

    // 自动完成
    protected $_auto=array(
        array('create_time','time',1,'function'), // 对date字段在新增的时候写入当前时间戳
    );


    public function getAdminList($param){
        $keyword = $param['keyword'];
        if (!empty($keyword)) {
            $map['name'] = array('like','%'.$keyword.'%');
        }
        $assign = $this->getAdminPage($this,$map,$param,'id desc');
        return $assign;
    }


	/**
     * 添加关键字回复
     */
    public function addData($data){
        // 对data数据进行验证
        if(!$data=$this->create($data)){
            // 验证不通过返回错误
            return false;
        }else{
            // 验证通过
            $result=$this->add($data);
            return $result;
        }
    }

    /**
     * 修改关键字回复
     */
    public function editData($map,$data){
        // 对data数据进行验证
        if(!$data=$this->create($data)){
            // 验证不通过返回错误
            return false;
        }else{
            // 验证通过
            $result=$this->where(array($map))->save($data);
            return $result;
        }
    }

    //获取关键字回复详情
    public function getData($map){
        $data = $this->where($map)->find();
        return $data;
    }
    
    /**
     * 删除数据
     * @param   array   $map    where语句数组形式
     * @return  boolean         操作是否成功
     */
    public function deleteData($map){
    //     $logo = $this->where($map)->getField('logo');
        $result = $this->where($map)->delete();
        if($result){
            // deleteFile($logo);
            return true;
        }else{
            return false;
        }
    }

    public function checkInterface($wechat){
        $openid = $wechat->getRevFrom();
        $content = $wechat->getRevContent();

        $array = S('interface');
        if(!is_null($array[$openid])){
            $map = $array[$openid];
            switch ($map['name']) {
                case 'searchDomain':
                    $this->searchDomain($wechat);
                    break;
            }
            return true;
        }
        return false;

    }
    public function searchDomain($wechat){
        $openid = $wechat->getRevFrom();
        $content = $wechat->getRevContent();
        $array = S('interface');
        //检测当前用户是否存在缓存
        if(!is_null($array[$openid])){
            $map = $array[$openid];
            //若用户输入0则退出
            if($content=="0"){
                S('interface',array($openid=>null));
                $wechat->text("已退出域名查询")->reply();
            }
            if($map['step']==1){
                Vendor('aliyun-php-sdk-core.Config', '' ,'.php');
                $iClientProfile = \DefaultProfile::getProfile("cn-hangzhou", "", "");
                $client = new \DefaultAcsClient($iClientProfile);
                $request = new \Domain\Request\V20160511\CheckDomainRequest();
                $subFix = array('com','cn','com.cn','net','org','cc');
                $str = "";
                foreach ($subFix as $key => $value) {
                    $domain = $content.'.'.$value;
                    $str .= $domain.'  ';
                    $request->setDomainName($domain);
                    $response = $client->getAcsResponse($request);
                    $str.= ($response->Avail==1?"<a color='green' href='http://".$domain."'>未注册</a> ":'已注册')."\n";
                }
                $str.="输入域名继续查询，输入0退出查询";
                $wechat->text($str)->reply();
            }
            
        }else{
            $map['name'] = 'searchDomain';
            $map['step'] = 1;
            S('interface',array($openid=>$map),300);
            $wechat->text("请输入要查询的域名\n比如：baidu。")->reply();
        }
    }



}
