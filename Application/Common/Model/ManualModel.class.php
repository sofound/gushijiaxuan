<?php
namespace Common\Model;
use Common\Model\BaseModel;
/**
 * ModelName
 */
class ManualModel extends BaseModel{
    // 自动验证
    protected $_validate=array(
        array('title','require','文章标题必须',0,'',1), // 验证字段必填
    );

    // 自动完成
    protected $_auto=array(
        array('create_time','time',1,'function'), // 对date字段在新增的时候写入当前时间戳
    );

    //获取后台文章分页列表
    public function getAdminList($param){
        $keywords = $param['keywords'];
        if (!empty($keywords)) {
            $map = 'title like "%'.$keywords.'%" or keywords like "%'.$keywords.'%"';
        }
        $assign = $this->getAdminPage($this,$map,$param,'id desc');
        return $assign;
    }

    /**
     * 添加文章
     */
    public function addData($data,$substr=false){
        // 对data数据进行验证
        if(!$data=$this->create($data)){
            // 验证不通过返回错误
            return false;
        }else{
            // 验证通过
            $data['content']=htmlspecialchars_decode($data['content']);
            $data['content']=preg_replace('/src=\"^\/.*\/Upload\/image\/ueditor$/','src="/Upload/image/ueditor',$data['content']);
            $data['content']=htmlspecialchars($data['content']);
            $result=$this->add($data);
            return $result;
        }
    }

    /**
     * 修改文章
     */
    public function editData($map,$data,$substr=false){
        // 对data数据进行验证
        if(!$data=$this->create($data)){
            // 验证不通过返回错误
            return false;
        }else{
            $data['content']=htmlspecialchars_decode($data['content']);
            $data['content']=preg_replace('/src=\"^\/.*\/Upload\/image\/ueditor$/','src="/Upload/image/ueditor',$data['content']);
            $data['content']=htmlspecialchars($data['content']);
            // 验证通过
            $result=$this
                ->where(array($map))
                ->save($data);
            return $result;
        }
    }

    /**
     * 获取文章详情
     */
    public function getData($map){
        $data = $this->where($map)->find();
        $data['content']=htmlspecialchars_decode($data['content']);
        $data['content']=preg_replace('/src=\"^\/.*\/Upload\/image\/ueditor$/','src="/Upload/image/ueditor',$data['content']);
        return $data;
    }

     /**
     * 删除数据
     * @param   array   $map    where语句数组形式
     * @return  boolean         操作是否成功
     */
    public function deleteData($map){
        $result = $this->where($map)->delete();
        return $result;
    }
}
