<?php
namespace Common\Model;
use Common\Model\BaseModel;
/**
 * ModelName
 */
class AdminModel extends BaseModel{
    // 自动验证
    protected $_validate=array(
        array('username','require','用户名必须',0,'',3), // 验证字段必填
    );

    // 自动完成
    protected $_auto=array(
        array('password','md5',1,'function') , // 对password字段在新增的时候使md5函数处理
        array('register_time','time',1,'function'), // 对date字段在新增的时候写入当前时间戳
    );

    /**
     * 添加用户
     */
    public function addData($data){
        // 对data数据进行验证
        if(!$data=$this->create($data)){
            // 验证不通过返回错误
            return false;
        }else{
            // 验证通过
            $result=$this->add($data);
            return $result;
        }
    }

    /**
     * 修改用户
     */
    public function editData($map,$data){
        // 对data数据进行验证
        if(!$data=$this->create($data)){
            // 验证不通过返回错误
            return false;
        }else{
            // 验证通过
            $result=$this
                ->where(array($map))
                ->save($data);
            return $result;
        }
    }

    /**
     * 删除数据
     * @param   array   $map    where语句数组形式
     * @return  boolean         操作是否成功
     */
    public function deleteData($map){
        die('禁止删除用户');
    }

    public function login(){
        $username = I('post.username','');
        $password = I('post.password','');
        $map['username'] = $username;
        $map['password'] = md5($password);
        if(!trim($username) || !trim($password)){
            return false;
        }
        $user = D('Admin');
        $where['username'] = $username;
        $where['password'] = md5($password);
        $res = $user->where($where)->find();
        if($res){
            $_SESSION['user'] = $res;
            return $res;
        }else{
            return false;
        }
    }

    public function logout(){
        unset($_SESSION['user']);
    }

    public function editPassword($data){
        $old_password = md5($data['old_password']);
        $user = $_SESSION['user'];
        if($old_password != $user['password']){
            return false;
        }else{
            $user['password'] = md5($data['new_password']);
            $result = $this->where('id='.$user['id'])->save($user);
            if($result){
                $_SESSION['user'] = $user;
                return true;
            }else{
                return false;
            }
        }
    }

}
