<?php
namespace Common\Model;
use Common\Model\BaseModel;
/**
 * 微信类配置
 */
class WechatModel extends BaseModel{
   public function getWechat(){
        $map['type'] = 'wechat';
        $data = D('Config')->getData($map,true);
        $options['token']               = $data['wechat_base_token'];
        $options['encodingaeskey']      = $data['wechat_base_encodingaeskey'];
        $options['appid']               = $data['wechat_base_appid'];
        $options['appsecret']           = $data['wechat_base_appsecret'];
        $options['debug']               = $data['wechat_base_debug'];
        $options['logcallback']         = $data['wechat_base_logcallback'];
        return new TPWechat($options);
   }

}
