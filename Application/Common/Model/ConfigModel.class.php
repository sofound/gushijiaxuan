<?php
namespace Common\Model;
use Common\Model\BaseModel;
/**
 * 菜单操作model
 */
class ConfigModel extends BaseModel{

	public function addData($data,$type){
		foreach ($data as $key => $value) {
			$config['name'] = $key;
			$config['value'] = $value;
			$config['type'] = $type;
			$map['name'] = $config['name'];
			$map['type'] = $config['type'];
			$oldData = $this->where($map)->find();
			if($oldData){
				$this->where($map)->setField('value', $config['value']);
			}else{
				$this->add($config);
			}
		}
	}

	public function getData($map){
		$config =  $this->where($map)->select();
        foreach ($config as $key => $value) {
           $data[$value['name']] = $value['value'];
        }
        if(isset($data['statistics_baidu'])){
        	$data['statistics_baidu'] = htmlspecialchars_decode($data['statistics_baidu']);
        }
        if(isset($data['statistics_360'])){
	        $data['statistics_360'] = htmlspecialchars_decode($data['statistics_360']);
        }
        if(isset($data['push_baidu'])){
        	$data['push_baidu'] = htmlspecialchars_decode($data['push_baidu']);
        }
        return $data;
	}
}
