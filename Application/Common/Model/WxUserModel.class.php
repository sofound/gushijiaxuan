<?php
namespace Common\Model;
use Common\Model\BaseModel;
/**
 * ModelName
 */
class WxUserModel extends BaseModel{
    // 自动验证
    protected $_validate=array(
        // array('title','require','文章标题必须',0,'',3), // 验证字段必填
    );

    // 自动完成
    protected $_auto=array(
        array('create_time','time',1,'function'), // 对date字段在新增的时候写入当前时间戳
    );


    /**
     * 添加微信用户
     */
    public function addData($data,$substr=false){
        // 对data数据进行验证
        if(!$data=$this->create($data)){
            // 验证不通过返回错误
            return false;
        }else{
            $map['openid'] = $data['openid'];
            $user = $this->getData($map);
            if(!empty($user)){
                $this->editData($map,$data);
            }else{
                $result=$this->add($data);
            }
            return $result;
        }
    }

    /**
     * 修改微信用户
     */
    public function editData($map,$data,$substr=false){
        // 对data数据进行验证
        if(!$data=$this->create($data)){
            // 验证不通过返回错误
            return false;
        }else{
            // 验证通过
            $result=$this->where(array($map))->save($data);
            return $result;
        }
    }

    /**
     * 获取微信用户
     */
    public function getData($map){
        $data = $this->where($map)->find();
        return $data;
    }

     /**
     * 删除微信用户
     * @param   array   $map    where语句数组形式
     * @return  boolean         操作是否成功
     */
    public function deleteData($map){
        $result = $this->where($map)->delete();
        if($result){
            return true;
        }else{
            return false;
        }
    }

}
