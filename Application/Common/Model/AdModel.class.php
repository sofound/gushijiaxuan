<?php
namespace Common\Model;
use Common\Model\BaseModel;
/**
 * ModelName
 */
class AdModel extends BaseModel{
    // 自动验证
    protected $_validate=array(
        array('name','require','广告名必须',0,'',3), // 验证字段必填
    );

    // 自动完成
    protected $_auto=array(
        array('create_time','time',1,'function'), // 对date字段在新增的时候写入当前时间戳
    );

    public function getAdminList($param){
        if($param['pid'] != null){
            $map['pid']=$param['pid'];
        }
        $assign = $this->getAdminPage($this,$map,$param,'id desc');
        foreach ($assign['data'] as $key => $value) {
            $assign['data'][$key]['category'] = D('AdPosition')->where('id='.$value['pid'])->getField('name');
        }
        return $assign;
    }

    /**
     * 添加广告
     */
    public function addData($data){
        // 对data数据进行验证
        if(!$data=$this->create($data)){
            // 验证不通过返回错误
            return false;
        }else{
            // 验证通过
            $result=$this->add($data);
            return $result;
        }
    }

    /**
     * 修改广告
     */
    public function editData($map,$data){
        // 对data数据进行验证
        if(!$data=$this->create($data)){
            // 验证不通过返回错误
            return false;
        }else{
            // 验证通过
            //如果上传了新logo，删除旧的logo
            $path = $this->where($map)->getField('path');
            if ($path != $data['path']) {
                deleteFile($path);
            }
            $result=$this
                ->where(array($map))
                ->save($data);
            return $result;
        }
    }

    /**
     * 删除数据
     * @param   array   $map    where语句数组形式
     * @return  boolean         操作是否成功
     */
    public function deleteData($map){
        $path = $this->where($map)->getField('path');
        $result = $this->where($map)->delete();
        if($result){
            deleteFile($path);
            return true;
        }else{
            return false;
        }
    }

    public function getData($map){
         $result = $this->where($map)->find();
         return $result;
    }

    public function getList($map, $limit=1, $order='id desc') {
        $result = $this->where($map)->limit($limit)->order($order)->select();
        return $result;
    }



}
