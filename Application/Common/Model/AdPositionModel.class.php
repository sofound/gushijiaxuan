<?php
namespace Common\Model;
use Common\Model\BaseModel;
/**
 * ModelName
 */
class AdPositionModel extends BaseModel{
    //自动验证
    protected $_validate=array(
        array('name','require','广告位置名必须',0,'',3), // 验证字段必填
    );

    // 自动完成
    protected $_auto=array(
        array('create_time','time',1,'function'), // 对date字段在新增的时候写入当前时间戳
    );

    /**
     * 添加广告位置
     */
    public function addData($data){
        // 对data数据进行验证
        if(!$data=$this->create($data)){
            // 验证不通过返回错误
            return false;
        }else{
            // 验证通过
            $result=$this->add($data);
            return $result;
        }
    }

    /**
     * 修改广告位置
     */
    public function editData($map,$data){
        // 对data数据进行验证
        if(!$data=$this->create($data)){
            // 验证不通过返回错误
            return false;
        }else{
            // 验证通过
            $result=$this
                ->where(array($map))
                ->save($data);
            return $result;
        }
    }

    /**
     * 删除数据
     * @param   array   $map    where语句数组形式
     * @return  boolean         操作是否成功
     */
    public function deleteData($map){
        $result = $this->where($map)->delete();
        if($result){
            return true;
        }else{
            return false;
        }
    }
}
