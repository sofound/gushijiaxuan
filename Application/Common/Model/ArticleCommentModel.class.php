<?php
namespace Common\Model;
use Common\Model\BaseModel;
/**
 * ModelName
 */
class ArticleCommentModel extends BaseModel{
    // 自动验证
    protected $_validate=array(
        array('title','require','文章标题必须',0,'',1), // 验证字段必填
    );

    // 自动完成
    protected $_auto=array(
        array('create_time','time',1,'function'), // 对date字段在新增的时候写入当前时间戳
    );

    //获取后台文章分页列表
    public function getAdminList($param){
        $keyword = $param['keyword'];
        $aid = $param['aid'];
        if (!empty($keyword)) {
            $map['content'] = array('like','%'.$keyword.'%');
        }
        if (!empty($aid)) {
            $map['aid'] = $aid;
        }
        $assign = $this->getAdminPage($this,$map,$param,'id desc');
        foreach ($assign['data'] as $key => $value) {
            $assign['data'][$key]['article'] = M('Article')->where('id='.$assign['data'][$key]['aid'])->getField('title');
        }
        return $assign;
    }

     /**
     * 删除数据
     * @param   array   $map    where语句数组形式
     * @return  boolean         操作是否成功
     */
    public function deleteData($map){
        $list = $this->where($map)->select();
        $result = $this->where($map)->delete();
        return $result;
    }

}
