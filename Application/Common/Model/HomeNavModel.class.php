<?php
namespace Common\Model;
use Common\Model\BaseModel;
/**
 * 菜单操作model
 */
class HomeNavModel extends BaseModel{

	public function addData($data){
        // 对data数据进行验证
        if(!$data=$this->create($data)){
            // 验证不通过返回错误
            return false;
        }else{
            // 验证通过
            $result=$this->add($data);
            return $result;
        }
    }

    public function editData($map,$data){
        // 对data数据进行验证
        if(!$data=$this->create($data)){
            // 验证不通过返回错误
            return false;
        }else{
            if($data['id'] != $data['pid']){
                $pid = $this->getPids($data['pid'],'0');
                if(strpos($pid,$data['id'])){
                    return false;
                }else{
                    $result=$this
                        ->where(array($map))
                        ->save($data);
                    return $result;
                }
            }else{
                return false;
            }
        }
    }

    /**
    * 递归获取所有父节点
    */
    public function getPids($pid,$str){
        $category = $this->where('id='.$pid)->find();
        if($category['pid'] == 0){
            return $str;
        }else{
            $str .= ','.$category['pid'];
            return $this->getPids($category['pid'],$str);
        }
    }

    /**
    * 递归获取所有子节点
    */
    public function getCids($id,$arr=array()){
        array_push($arr, $id);
        $children = $this->where('pid='.$id)->select();
        if(count($children) == 0){
            return $arr;
        }else{
            foreach ($children as $key => $value) {
                $arr = $this->getCids($value['id'], $arr);
            }
            return $arr;
        }
    }

	/**
	 * 删除数据
	 * @param	array	$map	where语句数组形式
	 * @return	boolean			操作是否成功
	 */
	public function deleteData($map){
		$count=$this
			->where(array('pid'=>$map['id']))
			->count();
		if($count!=0){
			return false;
		}
		$this->where(array($map))->delete();
		return true;
	}

	/**
	 * 获取全部菜单
	 * @param  string $type tree获取树形结构 level获取层级结构
	 * @return array       	结构数据
	 */
	public function getTreeData($type='tree',$order=''){
		// 判断是否需要排序
		if(empty($order)){
			$data=$this->select();
		}else{
			$data=$this->order('order_number is null,'.$order)->select();
		}
		// 获取树形或者结构数据
		if($type=='tree'){
			$data=\Org\Nx\Data::tree($data,'name','id','pid');
		}elseif($type="level"){
			$data=\Org\Nx\Data::channelLevel($data,0,'&nbsp;','id');
			// 显示有权限的菜单
			$auth=new \Think\Auth();
			foreach ($data as $k => $v) {
				if ($auth->check($v['mca'],$_SESSION['user']['id'])) {
					foreach ($v['_data'] as $m => $n) {
						if(!$auth->check($n['mca'],$_SESSION['user']['id'])){
							unset($data[$k]['_data'][$m]);
						}
					}
				}else{
					// 删除无权限的菜单
					unset($data[$k]);
				}
			}
		}
		// p($data);die;
		return $data;
	}


}
