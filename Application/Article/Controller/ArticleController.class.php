<?php
namespace Article\Controller;
use Common\Controller\HomeBaseController;
/**
 * 商城首页Controller
 */
class ArticleController extends HomeBaseController{

    public function _initialize(){
        parent::_initialize();
    }
    
    public function index(){
    	$this->assign('webpage','news');
    	$menulist = D('ArticleCategory')->where(array('pid'=>6))->select();
    	$this->assign('menulist',$menulist);
    	
    	$cid = I('cid');
    	if(!$cid){
    		$cid = 1;
    	}
    	$cdata = D('ArticleCategory')->where(array('id'=>$cid))->find();
    	$this->assign('cdata',$cdata);
    	$this->assign('cid',$cid);
    	$alist = D('Article')->order('release_time desc')->where(array('cid'=>$cid))->select();
    	$this->assign('alist',$alist);
    	$this->display('news');
    }

    public function newsInfo(){
    	$this->assign('webpage','news');
    	$id = I('get.id');
        $map['id'] = $id;
        $article = D('Article');
        //点击数+1
        $article->lookCount($map['id']);
        $data = $article->getData($map);
    	$exp = $data['expand'];
        if($exp[0]['name']=='来源'){
        	$data['laiyuan'] = $exp[0]['value'];
        }

        //当前分类、分类列表
        $menulist = M('ArticleCategory')->where('pid=6')->select();
        $this->assign('menulist',$menulist);
        $this->assign('cid',$data['cid']);
        $this->assign('data', $data);
        $this->display('newsinfo');
    }

    public function news(){
		$this->assign('webpage','news');
        $this->display('Home/news');
    }



}

