<?php
namespace User\Controller;
use Common\Controller\HomeBaseController;
class IndexController extends HomeBaseController{
 
    public function index(){
    	$id = $this->user_id;
    	$data = D('users')->where(array('id'=>$id))->find();
    	$this->assign('data',$data);
    	$this->assign('webpage','ucindex');
    	
    	//资产情况
    	$uid = $id;
		$myproduct = M('userproduct')->where(array('uid'=>$uid))->select();
		if(!empty($myproduct)){
			foreach ($myproduct as $k=>$v){
				$pid = $v['pid'];
				$pdata = M('Goods')->where(array('id'=>$pid))->find();
				$pname = $pdata['title'];
				$ptype = M('GoodsCategory')->where(array('id'=>$pdata['cateid']))->find();
				$ptype = $ptype['name'];
				
				$fene = $v['fene'];
				$zichan = $v['zichan'];
				
				$total += $zichan;
				
				$r[$ptype]['fene'] += $fene;
				$r[$ptype]['zichan'] += $zichan;
				$r[$ptype]['desc'] = $ptype;
			}
		}
		$this->assign('total',$total*0.0001);
		$this->assign('r',$r);    	
	    $this->display('User/index');
    }
    
    public function ccdetail(){
    	$id = $this->user_id;
    	$data = D('users')->where(array('id'=>$id))->find();
    	$this->assign('data',$data);
    	
    	$plist = D('Goods')->limit(0,3)->select();
    	
    	
    	//我的产品
    	$uid = $id;
		$myproduct = M('userproduct')->where(array('uid'=>$uid))->select();
		if(!empty($myproduct)){
			foreach ($myproduct as $k=>$v){
				$pid = $v['pid'];
				$pdata = M('Goods')->where(array('id'=>$pid))->find();
				$pname = $pdata['title'];
				$ptype = M('GoodsCategory')->where(array('id'=>$pdata['cateid']))->find();
				$ptype = $ptype['name'];
				$myproduct[$k]['pname'] = $pname;
				$myproduct[$k]['ptype'] = $ptype;
			}
		}
		$this->assign('myplist',$myproduct);
    	
    	$this->assign('plist',$plist);
    	$this->assign('webpage','ccdetail');
        $this->display('User/ccdetail');
    }
    
    public function jydetail(){
    	$id = $this->user_id;
    	$data = D('users')->where(array('id'=>$id))->find();
    	
    	//我的交易
    	$uid = $id;
		$myjy = M('userjy')->where(array('uid'=>$uid))->select();
		if(!empty($myjy)){
			foreach ($myjy as $k=>$v){
				$pid = $v['pid'];
				$pdata = M('Goods')->where(array('id'=>$pid))->find();
				$pname = $pdata['title'];
				$ptype = M('GoodsCategory')->where(array('id'=>$pdata['cateid']))->find();
				$ptype = $ptype['name'];
				$myjy[$k]['pname'] = $pname;
				$myjy[$k]['ptype'] = $ptype;
			}
		}
		$this->assign('myjy',$myjy);
    	
    	$this->assign('data',$data);
    	$this->assign('webpage','jydetail');
    	$this->display('User/jydetail');
    }
    
    public function cp(){
    	$id = $this->user_id;
    	$data = D('users')->where(array('id'=>$id))->find();
    	$pingfen = $data['pingfen'];
    	if($pingfen>=20 && $pingfen<=35){
    		$ph = '保守型';
    		$cp = '低风险';
    		$p = 1;
    	}
    	if($pingfen>35 && $pingfen<=50){
    		$ph = '稳健型';
    		$cp = '低风险和较低风险';
    		$p = 2;
    	}
    	if($pingfen>50 && $pingfen<=65){
    		$ph = '平衡型';
    		$cp = '低风险、较低风险和中风险';
    		$p = 3;
    	}
    	if($pingfen>65 && $pingfen<=80){
    		$ph = '成长型';
    		$cp = '低风险、较低风险、中风险和较高风险';
    		$p = 4;
    	}
    	if($pingfen>80 && $pingfen<=100){
    		$ph = '进取型';
    		$cp = '低风险、较低风险、中风险、较高风险和高风险等级';
    		$p = 5;
    	}
    	$this->assign('p',$p);
    	$this->assign('ph',$ph);
    	$this->assign('cp',$cp);
    	$this->assign('data',$data);
    	$this->assign('webpage','cp');
    	$this->display('User/cp');
    }
    

    public function cplist(){
    	$id = $this->user_id;
    	$data = D('users')->where(array('id'=>$id))->find();
    	$pingfen = $data['pingfen'];
    	if($pingfen>=20 && $pingfen<=35){
    		$ph = '保守型';
    		$cp = '低风险';
    	}
    	if($pingfen>35 && $pingfen<=50){
    		$ph = '稳健型';
    		$cp = '低风险和较低风险';
    	}
    	if($pingfen>50 && $pingfen<=65){
    		$ph = '平衡型';
    		$cp = '低风险、较低风险和中风险';
    	}
    	if($pingfen>65 && $pingfen<=80){
    		$ph = '成长型';
    		$cp = '低风险、较低风险、中风险和较高风险';
    	}
    	if($pingfen>80 && $pingfen<=100){
    		$ph = '进取型';
    		$cp = '低风险、较低风险、中风险、较高风险和高风险等级';
    	}
    	$this->assign('ph',$ph);
    	$this->assign('cp',$cp);
    	$this->assign('data',$data);
    	$this->assign('webpage','cp');
    	$this->display('User/cplist');
    }
    
    public function setInfo(){
    	$id =  $_SESSION['user_id'];
    	$phone = I('phone');
    	$name = I('name');
    	M('Users')->where(array('id'=>$id))->save(array('name'=>$name,'phone'=>$phone));
        $_SESSION['name'] = $name;
        $_SESSION['phone'] = $phone;
           
    	 $this->redirect('/User/Index/ucinfo');

    }
    
	public function setPwd(){
    	$id = $_SESSION['user_id'];
    	$oldpwd = I('oldpwd');
    	$pwd = I('pwd');
        $pwd_check=I('pwd_check');
        if($pwd!=$pwd_check){
            $msg = '两次密码输入不一致';
             $this->assign('msg',$msg);
             $this->display('User/ucinfoedit');
             exit;
        }
    	$res = M('Users')->where(array('id'=>$id,'password'=>md5($oldpwd)))->save(array('password'=>md5($pwd)));
    	if($res){
	    	$msg = '密码修改成功';
             $this->assign('msg',$msg);
             $this->display('User/ucinfoedit');
    	}else{
    		$msg = '原密码错误';
             $this->assign('msg',$msg);
             $this->display('User/ucinfoedit');
    	}
    }
    
    
    public function ucinfo(){
    	$id = $this->user_id;
    	$data = D('users')->where(array('id'=>$id))->find();
    	$this->assign('data',$data);
    	$this->assign('webpage','ucinfo');
    	$this->display('User/ucinfo');
    }
    
	public function ucinfoedit(){
    	$id = $this->user_id;
    	$data = D('users')->where(array('id'=>$id))->find();
    	$this->assign('data',$data);
    	$this->assign('webpage','ucinfo');
    	$this->display('User/ucinfoedit');
    }

 
  
	
    
    /**
     * 导表
     * Enter description here ...
     */
    public function importdata(){
//    	echo 'import';
    	//该日期
    	//M('userproduct')->where(array('pid'=>0))->save(array('pid'=>4));
//    	$list = M('userproduct')->select();
//    	foreach ($list as $v){
//    		$id = $v['id'];
//    		$d = $v['ctime'];
//    		$dd = strtr($d,array('年'=>'-','月'=>'-','日'=>''));
//    		echo $dd.'<br>';
//    	}
//    	echo '<pre>';
//    	print_r($list);
		//M('userproduct')->where(array('uid'=>0))->save(array('ctime'=>'2016-07-12'));
		//改UID
//    	$list = M('userproduct')->select();
//    	foreach ($list as $v){
//    		$id = $v['id'];
//    		$n = $v['uname'];
//    		$uid = M('users')->where(array('name'=>$n))->find();
//    		$uid = $uid['id'];
//    		M('userproduct')->where(array('id'=>$id))->save(array('uid'=>$uid));
//    		echo $uid.'<br>';
//    	}

//    	M('userjy')->where(array('pid'=>0))->save(array('pid'=>4));
//    	$list = M('userjy')->select();
//    	foreach ($list as $v){
//    		$id = $v['id'];
//    		$n = $v['uname'];
//    		$uid = M('users')->where(array('name'=>$n))->find();
//    		$uid = $uid['id'];
//    		M('userjy')->where(array('id'=>$id))->save(array('uid'=>$uid));
//    		echo $uid.'<br>';
//    	}
//		M('users')->where(array('status'=>'2'))->save(array('password'=>'96e79218965eb72c92a549dd5a330112'));
		
//		echo md5('111111');
    }
    

}
