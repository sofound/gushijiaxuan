<?php
namespace Home\Controller;
use Common\Controller\HomeBaseController;
/**
 * 首页Controller
 */
class IndexController extends HomeBaseController{

    public function _initialize(){
        parent::_initialize();
    }
    
    public function note(){
    	$this->display('Home/note');
    }
	/**
	 * 首页
	 */
	public function index(){
		//顶部广告
		$adlist = D('Ad')->select();
		$this->assign('adlist',$adlist);
		$this->assign('webpage','home');
		
		$cid=7;//服务
		$fwlist = D('Article')->order('sort desc')->where(array('cid'=>$cid))->limit(0,3)->select();
		$this->assign('fwlist',$fwlist);
		

		
		$aid = 3;
		$about = D('Article')->getData(array('id'=>$aid));
		$this->assign('about',$about);
//		
		$cid=1;//动态
		$dtlist = D('Article')->order('id desc')->where(array('cid'=>$cid))->limit(0,6)->select();
		$this->assign('dtlist',$dtlist);
//		
		$cid=2;//大事件
		$dsjlist = D('Article')->order('release_time desc')->where(array('cid'=>$cid))->limit(0,4)->select();
		if(!empty($dsjlist)){
			$dsjtop = array_shift($dsjlist);
			$this->assign('dsjtop',$dsjtop);
		}
		$this->assign('dsjlist',$dsjlist);
//		
        $this->display('Home/index');
	}

	public function about(){
		$aid = 3;
		$this->assign('aid',$aid);
		$about = D('Article')->getData(array('id'=>$aid));
		$this->assign('about',$about);
		$this->assign('webpage','about');
		$this->display('Home/about');
	}

	
	public function contact(){
		
		$this->assign('webpage','contact');
		$this->display('Home/contact');
	}
	
	public function service(){
		$this->assign('webpage','service');
		$servicelist = D('Article')->where(array('cid'=>7))->order('sort desc')->select();
		$id = I('id');
		if(!$id){
			$data = $servicelist[0];
			$id = $data['id'];
		}
		$data = D('Article')->getData(array('id'=>$id));
		$this->assign('data',$data);
		$this->assign('id',$id);
    	$this->assign('servicelist',$servicelist);
		
		
		
		$this->display('Home/service');
	}
	
	public function download(){
		$this->assign('webpage','download');
		$this->display('Home/download');
	}
	
	public function register(){
		$this->assign('webpage','ucenter');
		$this->display('Home/register');
	}
	
	public function message(){
		$this->assign('webpage','contact');
    	$this->display('Home/message');
    }
    
	public function postmsg(){
		$args = I('post.');
		
		$name = $args['name'];
		if(!$name){
			$msg = '请填写名字';
			$this->assign('msg',$msg);
			$this->assign('name',$name);
			$this->display('Home/message');
			exit;
		}
		$phone = $args['phone'];
		if(!$phone){
			$msg = '请填写电话';
			$this->assign('msg',$msg);
			$this->assign('name',$name);
			$this->assign('phone',$phone);
			$this->display('Home/message');
			exit;
		}
		$content = $args['content'];
		if(!$content){
			$msg = '请填写留言内容';
			$this->assign('msg',$msg);
			$this->assign('name',$name);
			$this->assign('phone',$phone);
			$this->assign('content',$content);
			$this->display('Home/message');
			exit;
		}
		$msg = '留言提交成功';
		$this->assign('msg',$msg);
		M('message')->add($args);
		
		$this->display('Home/message');
    }
    
	
	public function login(){
		$this->assign('webpage','ucenter');
		$this->display('Home/login');
	}
	
	public function news(){
		$cid = 3;
		$articlelist = D('Article')->where(array('cid'=>$cid))->order('id asc')->select();
		$this->assign('alist',$articlelist);
		$newslist = D('Article')->where(array('cid'=>7))->order('id asc')->select();
		$this->assign('nlist',$newslist);
		
		$this->assign('webpage','abnews');
		$this->display('Home/news');
	}
	
	public function newsInfo(){
		$cid = 3;
		$articlelist = D('Article')->where(array('cid'=>$cid))->order('id asc')->select();
		$this->assign('alist',$articlelist);
		
		$id = I('id');
		
		$data = D('Article')->getData(array('id'=>$id));
		
		$this->assign('data',$data);
		
		$this->assign('webpage','abnews');
		$this->display('Home/newsinfo');
	}
	

}

	