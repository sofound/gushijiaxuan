<?php
namespace Admin\Controller;
use Common\Controller\AdminBaseController;
/**
 * 文章管理
 */
class ArticleCommentController extends AdminBaseController{

    public function index(){
        $param = I('get.');
        $assign = D('ArticleComment')->getAdminList($param);
        $this->assign('assign',$assign);
        $this->display();
    }

    public function editAjax(){
        $id = I('post.id');
        $param = I('post.param');
        $result = D('ArticleComment')->where('id='.$id)->setField('status', $param);
        if($result){
            exit(json_encode(array('code'=>true)));
        }else{
            exit(json_encode(array('code'=>false)));
        }
    }

    public function delete(){
        $articleComment = D('ArticleComment');
        $map['id'] = I('get.id');
        if($articleComment->deleteData($map)){
            $this->success('删除成功');
        }else{
            $this->error('删除失败');
        }
    }

    public function batchDelete(){
        $articleComment = D('ArticleComment');
        $map['id'] = array('IN', I('get.ids'));
        if($articleComment->deleteData($map)){
            $this->success('删除成功');
        }else{
            $this->error('删除失败');
        }
    }
}
