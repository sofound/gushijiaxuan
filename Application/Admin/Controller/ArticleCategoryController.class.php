<?php
namespace Admin\Controller;
use Common\Controller\AdminBaseController;
/**
 * 文章管理
 */
class ArticleCategoryController extends AdminBaseController{

    public function index(){
        $type = (C('ARTICLE_CATEGORY_TYPE'));
        $data=D('ArticleCategory')->getTreeData('tree','');
        $this->assign('type', $type);
        $this->assign('data', $data);
        $this->display();
    }

    public function add(){
        if(IS_POST){
            $data = I('post.','');
            $ArticleCategory = D('ArticleCategory');
            if($ArticleCategory->addData()){
                $this->success('文章分类添加成功',U('Admin/ArticleCategory/index'));
            }else{
                $this->error('文章分类添加失败',U('Admin/ArticleCategory/index'));
            }
        }else{
            $pid = I('get.pid', 0);
            $allCategory=D('ArticleCategory')->getTreeData('tree','');
            $this->assign('pid', $pid);
            $this->assign('allCategory',$allCategory);
            $this->display();
        }
    }

    public function edit(){
        if(IS_POST){
            $data = I('post.','');
            $map['id'] = $data['id'];
            $ArticleCategory = D('ArticleCategory');
            if($ArticleCategory->editData($map,$data) !== false){
                $this->success('文章分类修改成功',U('Admin/ArticleCategory/index'));
            }else{
                $this->error('文章分类修改失败',U('Admin/ArticleCategory/index'));
            }
        }else{
            $map['id'] = I('get.id', 0);
            $data = D('ArticleCategory')->where($map)->find();
            $allCategory=D('ArticleCategory')->getTreeData('tree','');
            $expandMap['cid'] = $map['id']; 
            $expand = D('ArticleCategory')->getExpand($expandMap);
            $this->assign('expand', $expand);
            $this->assign('data', $data);
            $this->assign('allCategory',$allCategory);
            $this->display();
        }
    }

    public function editAjax(){
        $id = I('post.id');
        $type = I('post.type');
        $param = I('post.param');
        $result = D('ArticleCategory')->where('id='.$id)->setField($type, $param);
        if($result){
            exit(json_encode(array('code'=>true)));
        }else{
            exit(json_encode(array('code'=>false)));
        }
    }

    public function delete(){
        $map['id'] = I('get.id',0);
        $ArticleCategory = D('ArticleCategory');
        if($ArticleCategory->deleteData($map)){
            $this->success('文章分类删除成功',U('Admin/ArticleCategory/index'));
        }else{
            $this->error($ArticleCategory->getError());
        }
    }
}
