<?php
namespace Admin\Controller;
use Common\Controller\AdminBaseController;
/**
 * 文章管理
 */
class OrderController extends AdminBaseController{

    //文章列表
    public function index(){
        $param = I('get.');
        $assign = D('Order')->getAdminList($param);
        $list = $assign['data'];
        if(!empty($list)){
        	foreach ($list as $k=>$v){
        		$items = D('OrderItem')->where(array('order_id'=>$v['id']))->select();
        		$list[$k]['items'] = $items;
        	}
        	$assign['data'] = $list;
        }
        $this->assign('assign',$assign);
        $this->display();
    }

    // 修改
    public function edit(){
        if(IS_POST){
            $order = D('Order');
            $data = I('post.','');
            $map['id'] = $data['id'];
            $substr = $data['is_substr'];
            $result = $order->editData($map,$data,$substr);
            if($result !== false){
                $this->success('修改成功',U('Admin/Order/edit', array('id' => $data['id'])));
            }else{
                $this->error('修改失败',U('Admin/Order/edit', array('id' => $data['id'])));
            }
        }else{
            $map['id'] = I('get.id',0);
            $order = D('Order');
            $data = $order->getData($map);
            $this->assign('data',$data);
            $this->display();
        }
    }

    public function editAjax(){
        $id = I('post.id');
        $type = I('post.type');
        $param = I('post.param');
        $result = D('Order')->where('id='.$id)->setField($type, $param);
        if($result){
            exit(json_encode(array('code'=>true)));
        }else{
            exit(json_encode(array('code'=>false)));
        }
    }

    public function delete(){
        $order = D('Order');
        $map['id'] = I('get.id');
        if($order->deleteData($map)){
            $this->success('删除成功');
        }else{
            $this->error('删除失败');
        }
    }

    public function batchDelete(){
        $order = D('Order');
        $map['id'] = array('IN', I('get.ids'));
        if($order->deleteData($map)){
            $this->success('删除成功');
        }else{
            $this->error('删除失败');
        }
    }

    public function upload(){
        $type = I('get.type','image');
        ajax_upload('/Upload/'.$type);
    }

    public function delete_album(){
        $id = I('post.id',0);
        $path = M("OrderAlbum")->where('id='.$id)->getField('path');
        $result = M("OrderAlbum")->where('id='.$id)->delete();
        if($result){
            deleteFile($path);
            exit(json_encode(array('code' => 1, 'msg' => '删除成功')));
        }else{
            exit(json_encode(array('code' => 0, 'msg' => '删除失败')));
        }
    }

    public function delete_logo(){
        $path = I('post.path');
        $id = I('post.id');
        $result = M('Order')->where('id='.$id)->setField('pic_path','');
        if($result){
            deleteFile($path);
            exit(json_encode(array('code' => 1, 'msg' => '删除成功')));
        }else{
            exit(json_encode(array('code' => 0, 'msg' => '删除失败')));
        }
    }

    public function getExpandAjax(){
        $map['cid'] = I('post.cid');
        $map['aid'] = I('post.aid',0);
        $list = D('Order')->getExpand($map);
        $str = "";
        if($list){
            foreach ($list as $key => $value) {
                $str .= '<div class="line line-dashed b-b line-lg pull-in"></div>
                    <div class="form-group m-b-none m-l-none m-r-none">
                        <label class="col-sm-2 control-label">';
                $str .= $value['c_name'];
                $str .='</label>
                    <div class="col-sm-6 v-m">
                        <input name="name[]" type="hidden" value="';
                $str .= $value['c_name'];
                $str .= '"/>';
                $str .= '<input name="expand_id[]" type="hidden" value="';
                $str .= $value['expand_id'];
                $str .= '"/>';
                if($value['type'] == 'input'){
                    $str .= '<input type="text" class="form-control" name="value[]" data-form-un="1470118834084.755" value="';
                    $str .= $value['value'];
                    $str .= '">';
                }
                if($value['type'] == 'select'){
                    $str .= '<select name="value[]" class="form-control">';
                    foreach ($value['option'] as $k => $v) {
                        $str .= '<option value="';
                        $str .= $v;
                        if($v == $value['value']){
                            $str .= '" selected>';
                        }else{
                            $str .= '">';
                        }
                        $str .= $v;
                        $str .= '</option>';
                    }
                    $str .= '</select>';
                }
                $str .= ' </div>
                    </div>';
            }
        }
        $data = M('OrderCategory')->field('content,seo,attachment,album,address')->where('id='.$map['cid'])->find();
        exit(json_encode(array('data'=>$data,'str'=>$str)));
    }

    public function search(){
        $word = I('word');
        $map['title'] = array('like','%'.$word.'%');
        exit(json_encode(D('Order')->where($map)->field('id,title,title as label')->select()));
    }
}
