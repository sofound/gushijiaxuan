<?php
namespace Admin\Controller;
use Common\Controller\AdminBaseController;
/**
 * 文章管理
 */
class ManualController extends AdminBaseController{

    //文章列表
    public function index(){
        $param = I('get.');
        $assign = D('Manual')->getAdminList($param);
        $this->assign('assign',$assign);
        $this->display();
    }

    // 添加文章
    public function add(){
        if(IS_POST){
            $data = I('post.','');
            $manual = D('Manual');
            $substr = $data['is_substr'];
            $result = $manual->addData($data,$substr);
            if($result){
                $this->success('添加成功',U('Admin/manual/edit',array('id' => $result)));
            }else{
                $this->error('添加失败');
            }
        }else{
            $this->display();
        }
    }

    // 修改文章
    public function edit(){
        if(IS_POST){
            $manual = D('Manual');
            $data = I('post.','');
            $map['id'] = $data['id'];
            $result = $manual->editData($map,$data);
            if($result !== false){
                $this->success('修改成功',U('Admin/manual/edit', array('id' => $data['id'])));
            }else{
                $this->error('修改失败',U('Admin/manual/edit', array('id' => $data['id'])));
            }
        }else{
            $map['id'] = I('get.id',0);
            $manual = D('Manual');
            $data = $manual->getData($map);
            $this->assign('data',$data);
            $this->display();
        }
    }

    public function view(){
        $map['id'] = I('get.id',0);
        $manual = D('Manual');
        $data = $manual->getData($map);
        $this->assign('data',$data); 
        $this->display();
    }

    public function delete(){
        $manual = D('Manual');
        $map['id'] = I('get.id');
        if($manual->deleteData($map)){
            $this->success('删除成功');
        }else{
            $this->error('删除失败');
        }
    }

    public function batchDelete(){
        $manual = D('Manual');
        $map['id'] = array('IN', I('get.ids'));
        if($manual->deleteData($map)){
            $this->success('删除成功');
        }else{
            $this->error('删除失败');
        }
    }

}
