<?php
namespace Admin\Controller;
use Common\Controller\AdminBaseController;
/**
 * 管理
 */
class GoodsCategoryController extends AdminBaseController{

    public function index(){
        $type = (C('ARTICLE_CATEGORY_TYPE'));
        $data=D('GoodsCategory')->getTreeData('tree','');
        $this->assign('type', $type);
        $this->assign('data', $data);
        $this->display();
    }

    public function add(){
        if(IS_POST){
            $data = I('post.','');
            $GoodsCategory = D('GoodsCategory');
            if($GoodsCategory->addData()){
                $this->success('分类添加成功',U('Admin/GoodsCategory/index'));
            }else{
                $this->error('分类添加失败',U('Admin/GoodsCategory/index'));
            }
        }else{
            $pid = I('get.pid', 0);
            $allCategory=D('GoodsCategory')->getTreeData('tree','');
            $this->assign('pid', $pid);
            $this->assign('allCategory',$allCategory);
            $this->display();
        }
    }

    public function edit(){
        if(IS_POST){
            $data = I('post.','');
            $map['id'] = $data['id'];
            $GoodsCategory = D('GoodsCategory');
            if($GoodsCategory->editData($map,$data) !== false){
                $this->success('分类修改成功',U('Admin/GoodsCategory/index'));
            }else{
                $this->error('分类修改失败',U('Admin/GoodsCategory/index'));
            }
        }else{
            $map['id'] = I('get.id', 0);
            $data = D('GoodsCategory')->where($map)->find();
            $allCategory=D('GoodsCategory')->getTreeData('tree','');
            $expandMap['cid'] = $map['id']; 
            $expand = D('GoodsCategory')->getExpand($expandMap);
            $this->assign('expand', $expand);
            $this->assign('data', $data);
            $this->assign('allCategory',$allCategory);
            $this->display();
        }
    }

    public function editAjax(){
        $id = I('post.id');
        $type = I('post.type');
        $param = I('post.param');
        $result = D('GoodsCategory')->where('id='.$id)->setField($type, $param);
        if($result){
            exit(json_encode(array('code'=>true)));
        }else{
            exit(json_encode(array('code'=>false)));
        }
    }

    public function delete(){
        $map['id'] = I('get.id',0);
        $GoodsCategory = D('GoodsCategory');
        if($GoodsCategory->deleteData($map)){
            $this->success('分类删除成功',U('Admin/GoodsCategory/index'));
        }else{
            $this->error($GoodsCategory->getError());
        }
    }
}
