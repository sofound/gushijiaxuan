<?php
namespace Admin\Controller;
use Common\Controller\AdminBaseController;
/**
 * 文章管理
 */
class AdController extends AdminBaseController{

    public function index(){
        $param = I('post.');
        $assign = D('Ad')->getAdminList($param);
        $list = D('AdPosition')->select();
        $this->assign('list', $list);
        $this->assign('assign',$assign);
    	$this->display();
    }

    public function add(){
    	if (IS_POST) {
    		$data = I('post.','');
			$ad = D('Ad');
			if($ad->addData($data)){
	            $this->success('添加成功',U('Admin/Ad/index'));
	        }else{
	            $this->error('添加失败');
	        }
	    }else{
            $count=D('AdPosition')->count();
            if($count == 0){
                $this->error('请先添加广告位置');
            }
            $position = D('AdPosition')->select();
            $this->assign('position', $position);
	    	$this->display();
    	}
    }

    public function edit(){
    	if (IS_POST) {
    		$data = I('post.','');
			$ad = D('Ad');
            $map['id'] = $data['id'];
			if($ad->editData($map,$data) !== false){
	            $this->success('修改成功',U('Admin/Ad/index'));
	        }else{
	            $this->error('修改失败');
	        }
    	}else{
    		$ad = D('Ad');
    		$id = I('get.id',0);
    		$data = $ad->where('id='.$id)->find();
            $position = D('AdPosition')->select();
            $this->assign('position', $position);
    		$this->assign('data', $data);
    		$this->display();
    	}
    }

    public function editAjax(){
        $id = I('post.id');
        $type = I('post.type');
        $param = I('post.param');
        $result = D('Ad')->where('id='.$id)->setField($type, $param);
        if($result){
            exit(json_encode(array('code'=>true)));
        }else{
            exit(json_encode(array('code'=>false)));
        }
    }

    public function delete(){
    	$id = I('get.id',0);
		$ad = D('Ad');
		if($ad->where('id='.$id)->delete()){
			$this->success('删除成功',U('Admin/Ad/index'));
		}else{
			$this->error('删除失败');
		}
    }

    public function batchDelete(){
        $ids = I('get.ids');
        $map['id'] = array('IN', $ids);
        $result = D('Ad')->where($map)->delete();
        if($result){
            $this->success('删除成功');
        }else{
            $this->error('删除失败');
        }
    }

    public function ajax_upload(){
    	ajax_upload('/Upload/image/');
    }

    public function positionList(){
        $data = D('AdPosition')->select();
        $this->assign('data', $data);
        $this->display();
    }

    public function addPosition(){
        $data = I('post.', '');
        if(D('AdPosition')->addData($data)){
            $this->success('添加成功',U('Admin/Ad/positionList'));
        }else{
            $this->success('添加失败');
        }
    }

    public function editPosition(){
        $data = I('post.', '');
        $map['id'] = $data['id'];
        if(D('AdPosition')->editData($map,$data)){
            $this->success('修改成功',U('Admin/Ad/positionList'));
        }else{
            $this->success('修改失败');
        }
    }

    public function deletePosition(){
        $id = I('get.id',0);
        if(D('AdPosition')->where('id='.$id)->delete()){
            $this->success('删除成功',U('Admin/Ad/positionList'));
        }else{
            $this->error('删除失败');
        }
    }
}
