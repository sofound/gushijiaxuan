<?php
namespace Admin\Controller;
use Common\Controller\AdminBaseController;
/**
 * 文章管理
 */
class ArticleController extends AdminBaseController{

    //文章列表
    public function index(){
        $param = I('get.');
        $assign = D('Article')->getAdminList($param);
        $allCategory=D('ArticleCategory')->getTreeData('tree','');
        $this->assign('allCategory',$allCategory);
        $this->assign('assign',$assign);
        $this->display();
    }

    // 添加文章
    public function add(){
        if(IS_POST){
            $data = I('post.','');
            $article = D('Article');
            $substr = $data['is_substr'];
            $release_time=$data['release_time'];
            
            $result = $article->addData($data,$substr);
            if($result){
                foreach ($data['album_name'] as $key => $value) {
                    $album['name'] = $value;
                    $album['path'] = $data['album_path'][$key];
                    $album['aid'] = $result;
                    M('ArticleAlbum')->add($album);
                }
                $this->success('添加成功',U('Admin/Article/edit',array('id' => $result)));
            }else{
                $this->error('添加失败');
            }
        }else{
            $count=D('ArticleCategory')->count();
            if($count == 0){
                $this->error('请先添加分类');
            }
            $allCategory=D('ArticleCategory')->getTreeData('tree','');
            $this->assign('allCategory',$allCategory);
            $this->display();
        }
        
    }

    // 向同步百度推送
    public function baidu_site($aid){
        $urls=array();
        $urls[]=U('Home/Index/article',array('aid'=>$aid),'',true);
        $api=C('BAIDU_SITE_URL');
        $ch=curl_init();
        $options=array(
            CURLOPT_URL=>$api,
            CURLOPT_POST=>true,
            CURLOPT_RETURNTRANSFER=>true,
            CURLOPT_POSTFIELDS=>implode("\n", $urls),
            CURLOPT_HTTPHEADER=>array('Content-Type: text/plain'),
        );
        curl_setopt_array($ch, $options);
        $result=curl_exec($ch);
        $msg=json_decode($result,true);
        if($msg['code']==500){
            curl_exec($ch);
        }
        curl_close($ch);
    }

    // 修改文章
    public function edit(){
        if(IS_POST){
            $article = D('Article');
            $data = I('post.','');
            $map['id'] = $data['id'];
            $substr = $data['is_substr'];
            $result = $article->editData($map,$data,$substr);
            if($result !== false){
                foreach ($data['album_name'] as $key => $value) {
                    $album['name'] = $value;
                    $album['path'] = $data['album_path'][$key];
                    $album['aid'] = $data['id'];
                    M('ArticleAlbum')->add($album);
                }
                $this->success('修改成功',U('Admin/Article/edit', array('id' => $data['id'])));
            }else{
                $this->error('修改失败',U('Admin/Article/edit', array('id' => $data['id'])));
            }
        }else{
            $map['id'] = I('get.id',0);
            $article = D('Article');
            $data = $article->getData($map);
            $allCategory=D('ArticleCategory')->getTreeData('tree','');
            $this->assign('allCategory',$allCategory);
            $this->assign('data',$data);
            $this->display();
        }
    }

    public function editAjax(){
        $id = I('post.id');
        $type = I('post.type');
        $param = I('post.param');
        $result = D('Article')->where('id='.$id)->setField($type, $param);
        if($result){
            exit(json_encode(array('code'=>true)));
        }else{
            exit(json_encode(array('code'=>false)));
        }
    }

    public function delete(){
        $article = D('Article');
        $map['id'] = I('get.id');
        if($article->deleteData($map)){
            $this->success('删除成功');
        }else{
            $this->error('删除失败');
        }
    }

    public function batchDelete(){
        $article = D('Article');
        $map['id'] = array('IN', I('get.ids'));
        if($article->deleteData($map)){
            $this->success('删除成功');
        }else{
            $this->error('删除失败');
        }
    }

    public function upload(){
        $type = I('get.type','image');
        ajax_upload('/Upload/'.$type);
    }

    public function delete_album(){
        $id = I('post.id',0);
        $path = M("ArticleAlbum")->where('id='.$id)->getField('path');
        $result = M("ArticleAlbum")->where('id='.$id)->delete();
        if($result){
            deleteFile($path);
            exit(json_encode(array('code' => 1, 'msg' => '删除成功')));
        }else{
            exit(json_encode(array('code' => 0, 'msg' => '删除失败')));
        }
    }

    public function delete_logo(){
        $path = I('post.path');
        $id = I('post.id');
        $result = M('Article')->where('id='.$id)->setField('pic_path','');
        if($result){
            deleteFile($path);
            exit(json_encode(array('code' => 1, 'msg' => '删除成功')));
        }else{
            exit(json_encode(array('code' => 0, 'msg' => '删除失败')));
        }
    }

    public function getExpandAjax(){
        $map['cid'] = I('post.cid');
        $map['aid'] = I('post.aid',0);
        $list = D('Article')->getExpand($map);
        $str = "";
        if($list){
            foreach ($list as $key => $value) {
                $str .= '<div class="line line-dashed b-b line-lg pull-in"></div>
                    <div class="form-group m-b-none m-l-none m-r-none">
                        <label class="col-sm-2 control-label">';
                $str .= $value['c_name'];
                $str .='</label>
                    <div class="col-sm-6 v-m">
                        <input name="name[]" type="hidden" value="';
                $str .= $value['c_name'];
                $str .= '"/>';
                $str .= '<input name="expand_id[]" type="hidden" value="';
                $str .= $value['expand_id'];
                $str .= '"/>';
                if($value['type'] == 'input'){
                    $str .= '<input type="text" class="form-control" name="value[]" data-form-un="1470118834084.755" value="';
                    $str .= $value['value'];
                    $str .= '">';
                }
                if($value['type'] == 'select'){
                    $str .= '<select name="value[]" class="form-control">';
                    foreach ($value['option'] as $k => $v) {
                        $str .= '<option value="';
                        $str .= $v;
                        if($v == $value['value']){
                            $str .= '" selected>';
                        }else{
                            $str .= '">';
                        }
                        $str .= $v;
                        $str .= '</option>';
                    }
                    $str .= '</select>';
                }
                $str .= ' </div>
                    </div>';
            }
        }
        $data = M('ArticleCategory')->field('content,seo,attachment,album,address')->where('id='.$map['cid'])->find();
        exit(json_encode(array('data'=>$data,'str'=>$str)));
    }

    public function search(){
        $word = I('word');
        $map['title'] = array('like','%'.$word.'%');
        exit(json_encode(D('Article')->where($map)->field('id,title,title as label')->select()));
    }
}
