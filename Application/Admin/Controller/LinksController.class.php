<?php
namespace Admin\Controller;
use Common\Controller\AdminBaseController;
/**
 * 后台友情连接管理
 */
class LinksController extends AdminBaseController{
    /**
     * 友情连接列表
     */
    public function index(){
        $param = I('get.');
        $data = D('Links')->getAdminList($param);
        $this->assign('keyword', $keyword);
        $this->assign('data', $data['data']);
        $this->assign('page', $data['page']);
        $this->display();
    }

    /**
     * 添加友情连接
     */
    public function add(){
        if (IS_POST) {
            $data=I('post.');
            if (D('Links')->addData($data)) {
                $this->success('添加成功',U('Admin/Links/index')); 
            }else{
                $this->error('添加失败');
            }
        }else{
            $this->display();
        }
        
    }

    /**
     * 修改友情连接
     */
    public function edit(){
        if (IS_POST) {
            $data = I('post.');
            $map['id'] = $data['id'];
            if(D('Links')->editData($map,$data) !== false){
                $this->success('修改成功',U('Admin/Links/index'));
            }else{
                $this->error('修改失败');
            }
        }else{                                                                                                                                                                         
            $map['id'] = I('get.id',0);
            $data = D('Links')->getData($map);        
            $this->assign('data', $data);
            $this->display();
        }
    }

    public function editAjax(){
        $id = I('post.id');
        $type = I('post.type');
        $param = I('post.param');
        $result = D('Links')->where('id='.$id)->setField($type, $param);
        if($result){
            exit(json_encode(array('code'=>true)));
        }else{
            exit(json_encode(array('code'=>false)));
        }
    }

    /**
     * 删除友情连接
     */
    public function delete(){
        $map['id'] = I('get.id');
        $result=D('Links')->deleteData($map);
        if($result){
            $this->success('删除成功',U('Admin/Links/index'));
        }else{
            $this->error('删除成功');
        }
    }

    public function batchDelete(){
        $ids = I('get.ids');
        $map['id'] = array('IN', $ids);
        $result = D('Links')->where($map)->delete();
        if($result){
            $this->success('删除成功');
        }else{
            $this->error('删除失败');
        }
    }
}
