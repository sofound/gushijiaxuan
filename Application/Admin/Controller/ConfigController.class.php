<?php
namespace Admin\Controller;
use Common\Controller\AdminBaseController;
/**
 * 文章管理
 */
class ConfigController extends AdminBaseController{

    public function webInfo(){
        $map['type'] = 'webInfo';
        $data = D('Config')->getData($map);
        $nav = D('AdminNav')->getTreeData('level','order_number,id');
        foreach ($nav as $key => $value) {
            foreach ($value['_data'] as $k => $v) {
                $list[] = $v;
            }
        }
        $this->assign('list', $list);
        $this->assign('data', $data);
    	$this->display();
    }

    public function save(){
        $data = I('post.');
        $type = I('get.type');
        D('Config')->addData($data,$type);
        $this->success('修改成功');
    }

    public function upload(){
        $type = I('get.type','image');
        ajax_upload('/Upload/'.$type);
    }
}
