<?php
namespace Admin\Controller;
use Common\Controller\AdminBaseController;
/**
 * 后台首页控制器
 */
class AdminController extends AdminBaseController{

	/**
	 * 用户列表
	 */
	public function index(){
		$word=I('get.word','');
		if (!empty($word)) {
			$map['username'] = array('like','%'.$word.'%');
		}
		$user = D('Admin');
		$assign = $user->getAdminPage($user,$map,$param,'id desc');
		$this->assign('assign',$assign);
		$this->display();
	}

	public function add(){
		$data = I('post.','');
		$user = D('Admin');
		if($user->addData($data)){
            $this->success('添加成功',U('Admin/Admin/index'));
        }else{
            $this->error('添加失败',U('Admin/Admin/index'));
        }
	}

	public function edit(){
		$data = I('post.','');
		$user = D('Admin');
		$map = array('id'=>$data['id']);
		if($user->editData($map,$data)){
            $this->success('修改成功',U('Admin/Admin/index'));
        }else{
            $this->error('修改失败',U('Admin/Admin/index'));
        }
	}

	public function delete(){
		$id = I('get.id',0);
		$user = D('Admin');
		if($user->where('id='.$id)->delete()){
			 $this->success('删除成功',U('Admin/Admin/index'));
		}else{
			$this->error('删除失败',U('Admin/Admin/index'));
		}
	}

	public function editPassword(){
		$data = I('post.','');
		$user = D('Admin');
		if($user->editPassword($data)){
            $this->success('修改成功',U('Admin/Index/index'));
        }else{
            $this->error('修改失败',U('Admin/Index/index'));
        }
	}

}
