<?php
namespace Admin\Controller;
use Common\Controller\AdminBaseController;
/**
 * 后台留言管理
 */
class MessageController extends AdminBaseController{
    /**
     * 留言列表
     */
    public function index(){
        $message = D('Message');
        $assign = $message->getAdminPage($message,$map,$param,'id desc');
        $this->assign('assign',$assign);
        $this->display();
    }

    /**
     * 删除留言
     */
    public function delete(){
        $id=I('get.id');
        $map=array(
            'id'=>$id
            );
        $result=D('Message')->where($map)->delete();
        if($result){
            $this->success('删除成功');
        }else{
            $this->error('删除失败');
        }
    }
}
