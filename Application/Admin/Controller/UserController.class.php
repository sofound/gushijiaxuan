<?php
namespace Admin\Controller;
use Common\Controller\AdminBaseController;
/**
 * 后台首页控制器
 */
class UserController extends AdminBaseController{

	/**
	 * 用户列表
	 */
	public function index(){
		$word=I('get.word','');
		if (!empty($word)) {
			$map['username'] = array('like','%'.$word.'%');
		}
		$user = D('Users');
		$assign = $user->getAdminPage($user,$map,$param,'id desc');
		$this->assign('assign',$assign);
		$this->display();
	}

	public function add(){
		$data = I('post.','');
		$user = D('Users');
		if($user->addData($data)){
            $this->success('添加成功',U('Admin/User/index'));
        }else{
            $this->error('添加失败',U('Admin/User/index'));
        }
	}

	public function edit(){
		$data = I('post.','');
		$user = D('Users');
		$map = array('id'=>$data['id']);
		if($user->editData($map,$data)){
            $this->success('修改成功',U('Admin/User/index'));
        }else{
            $this->error('修改失败',U('Admin/User/index'));
        }
	}

	public function delete(){
		$id = I('get.id',0);
		$user = D('Users');
		if($user->where('id='.$id)->delete()){
			 $this->success('删除成功',U('Admin/User/index'));
		}else{
			$this->error('删除失败',U('Admin/User/index'));
		}
	}

	public function editPassword(){
		$data = I('post.','');
		$user = D('Users');
		if($user->editPassword($data)){
            $this->success('修改成功',U('Admin/Index/index'));
        }else{
            $this->error('修改失败',U('Admin/Index/index'));
        }
	}
	
	//产品添加
	public function addProduct(){
		$uid = I('id');
		$this->assign('uid',$uid);
		$plist = D('Goods')->select();
		$this->assign('plist',$plist);
		$this->display();
	}
	
	//产品添加
	public function addProductAction(){
		$args = I('post.');
		M('userproduct')->add($args);
		$this->success('添加成功',U('Admin/User/productlist',array('id'=>$args['uid'])));
	}
	
	//产品添加
	public function productList(){
		$uid = I('id');
		$this->assign('uid',$uid);
		$myproduct = M('userproduct')->where(array('uid'=>$uid))->select();
		if(!empty($myproduct)){
			foreach ($myproduct as $k=>$v){
				$pid = $v['pid'];
				$pdata = M('Goods')->where(array('id'=>$pid))->find();
				$pname = $pdata['title'];
				$ptype = M('GoodsCategory')->where(array('id'=>$pdata['cateid']))->find();
				$ptype = $ptype['name'];
				$myproduct[$k]['pname'] = $pname;
				$myproduct[$k]['ptype'] = $ptype;
			}
		}
		$this->assign('myplist',$myproduct);
		$this->display();
	}
	
	
	
	//交易添加
	public function addjy(){
		$uid = I('id');
		$this->assign('uid',$uid);
		$plist = D('Goods')->select();
		$this->assign('plist',$plist);
		$this->display();
	}
	
	//交易添加
	public function addjyAction(){
		$args = I('post.');
		M('userjy')->add($args);
		$this->success('添加成功',U('Admin/User/jylist',array('id'=>$args['uid'])));
	}
	
	//交易添加
	public function jyList(){
		$uid = I('id');
		$this->assign('uid',$uid);
		$myproduct = M('userjy')->where(array('uid'=>$uid))->select();
		if(!empty($myproduct)){
			foreach ($myproduct as $k=>$v){
				$pid = $v['pid'];
				$pdata = M('Goods')->where(array('id'=>$pid))->find();
				$pname = $pdata['title'];
				$ptype = M('GoodsCategory')->where(array('id'=>$pdata['cateid']))->find();
				$ptype = $ptype['name'];
				$myproduct[$k]['pname'] = $pname;
				$myproduct[$k]['ptype'] = $ptype;
			}
		}
		$this->assign('myplist',$myproduct);
		$this->display();
	}

}
