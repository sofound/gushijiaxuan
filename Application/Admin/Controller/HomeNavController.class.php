<?php
namespace Admin\Controller;
use Common\Controller\AdminBaseController;
/**
 * 首页导航管理
 */
class HomeNavController extends AdminBaseController{

    public function index(){
        $type = (C('ARTICLE_CATEGORY_TYPE'));
        $data=D('HomeNav')->getTreeData('tree','');
        foreach ($data as $key => $value) {
            foreach ($type as $k => $v) {
                if($v['value'] == $value['type']){
                    $data[$key]['type_name'] = $v['name'];
                    break;
                }
            }
        }
        $this->assign('type', $type);
        $this->assign('data', $data);
        $this->display();
    }

    public function add(){
        if(IS_POST){
            $data = I('post.','');
            $homeNav = D('HomeNav');
            if($homeNav->addData()){
                $this->success('首页导航添加成功',U('Admin/HomeNav/index'));
            }else{
                $this->error('首页导航添加失败',U('Admin/HomeNav/index'));
            }
        }else{
            $pid = I('get.pid', 0);
            $allCategory=D('HomeNav')->getTreeData('tree','');
            $type = (C('ARTICLE_CATEGORY_TYPE'));
            $this->assign('type', $type);
            $this->assign('pid', $pid);
            $this->assign('allCategory',$allCategory);
            $this->display();
        }
    }

    public function edit(){
        if(IS_POST){
            $data = I('post.','');
            $map['id'] = $data['id'];
            $homeNav = D('HomeNav');
            if($homeNav->editData($map,$data) !== false){
                $this->success('首页导航修改成功',U('Admin/HomeNav/index'));
            }else{
                $this->error('首页导航修改失败',U('Admin/HomeNav/index'));
            }
        }else{
            $map['id'] = I('get.id', 0);
            $data = D('HomeNav')->where($map)->find();
            $allCategory=D('HomeNav')->getTreeData('tree','');
            $type = (C('ARTICLE_CATEGORY_TYPE'));
            $this->assign('type', $type);
            $this->assign('data', $data);
            $this->assign('allCategory',$allCategory);
            $this->display();
        }
    }

    public function delete(){
        $map['id'] = I('get.id',0);
        $homeNav = D('HomeNav');
        if($homeNav->deleteData($map)){
            $this->success('首页导航删除成功',U('Admin/HomeNav/index'));
        }else{
            $this->error($homeNav->getError());
        }
    }
}
