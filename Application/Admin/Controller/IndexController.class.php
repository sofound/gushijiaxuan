<?php
namespace Admin\Controller;
use Common\Controller\AdminBaseController;
/**
 * 后台首页控制器
 */
class IndexController extends AdminBaseController{
	/**
	 * 首页
	 */
	public function index(){
		// 分配菜单数据
		$user = $_SESSION['user'];
		if($user != null){
			$nav_data=D('AdminNav')->getTreeData('level','order_number,id');
			$cate_data=D('ArticleCategory')->getTreeData('level','order_number desc,id desc');
			$manual_data=M('Manual')->select();
			// var_dump($nav_data);
			$assign=array(
				'cate'=>$cate_data,
				'data'=>$nav_data,
				'manual'=>$manual_data,
				);
			$this->assign($assign);
			$this->display();
		}else{
			$this->display('login');
		}
	}
	/**
	 * elements
	 */
	public function elements(){

		$this->display();
	}
	
	/**
	 * welcome
	 */
	public function welcome(){
	    $this->display();
	}

	public function login(){
		if(IS_POST){
			$user = D('Admin');
			if($user->login()){
  				$this->success('登录成功，正在为您跳转到操作页面',U('Admin/Index/index'));
	        }else{
	            $this->error('用户名密码错误，请重新输入',U('Admin/Index/login'));
	        } 
		}
		else{
			$this->display();
		}
	}

	public function logout(){
		$user = D('Admin');
		$user->logout();
		$this->success('已经为您退出系统',U('Admin/Index/index'));
	}

	public function test(){
		$id = I('post.pk');
		$name = I('post.name');
		$value = I('post.value');
		$map['id'] = $id;
		$result = M('AdminNav')->where($map)->setField($name,$value);
		if($result){
			exit(json_encode(1));
		}else{
			exit(json_encode(0));
		}
	}
}
