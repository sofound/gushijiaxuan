<?php
namespace Admin\Model;
use Common\Model\BaseModel;
/**
 * ModelName
 */
class OrderModel extends BaseModel{

    public function getAdminList($param){
        $keyword = $param['keyword'];
        $status = $param['status'];
        if (!empty($keyword)) {
            $map['name'] = array('like','%'.$keyword.'%');
        }
        if (!empty($status)) {
        	$map['status'] = $status;
        }
        $assign = $this->getAdminPage($this,$map,$param,'id desc');
        return $assign;
    }


    /**
     * 修改文章
     */
    public function editData($map,$data,$substr=false){
        // 对data数据进行验证
        if(!$data=$this->create($data)){
            // 验证不通过返回错误
            return false;
        }else{
            //如果上传了新logo，删除旧的logo
            $oldPath = $this->where($map)->getField('cover');
            if ($oldPath != $data['cover']) {
                deleteFile($oldPath);
            }
            $data['content']=htmlspecialchars_decode($data['content']);
            if($substr == true && $data['info'] == null){
                $data['info'] = re_substr(strip_tags($data['content']),0,199);
            }
            if($data['is_hot'] == null){
                $data['is_hot'] = 0;
            } 
            if($data['is_top'] == null){
                $data['is_top'] = 0;
            }
            if($data['is_show'] == null){
                $data['is_show'] = 0;
            }
            if($data['is_original'] == null){
                $data['is_original'] = 0;
            }
            $data['content']=preg_replace('/src=\"^\/.*\/Upload\/image\/ueditor$/','src="/Upload/image/ueditor',$data['content']);
            $data['content']=htmlspecialchars($data['content']);
            if($data['release_time'] == null){
                $data['release_time'] = time();
            }else{
                $data['release_time'] = strtotime($data['release_time']);
            }
            // 验证通过
            $result=$this
                ->where(array($map))
                ->save($data);
            //$this->addExpand(I('post.'), $data['id']);
            return $result;
        }
    }

    /**
     * 获取详情
     */
    public function getData($map){
        $data = $this->where($map)->find();
        $items = M('OrderItem')->where(array('order_id'=>$data['id']))->select();
        $data['items'] = $items;
        return $data;
    }

     /**
     * 删除数据
     * @param   array   $map    where语句数组形式
     * @return  boolean         操作是否成功
     */
    public function deleteData($map){
        $list = $this->where($map)->select();
        $result = $this->where($map)->delete();
        if($result){
            foreach ($list as $key => $value) {
                //删除logo
                deleteFile($value['pic_path']);  
                //删除相册
                $albumList = M('OrderAlbum')->where('aid='.$value['id'])->select();
                M('OrderAlbum')->where('aid='.$value['id'])->delete();
                foreach ($albumList as $k => $v) {
                    deleteFile($v['path']);
                }
                //删除拓展字段
                //M('OrderExpand')->where('aid='.$value['id'])->delete();
            }
            return true;
        }else{
            return false;
        }
    }
}
